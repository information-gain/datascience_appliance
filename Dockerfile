FROM ubuntu:18.04
MAINTAINER Andrew Chisholm <andrew.chisholm@information-gain.com>
ARG DEBIAN_FRONTEND=noninteractive 
ARG CRAN_MIRROR=https://cloud.r-project.org 

RUN \
apt-get update -qq && \
apt-get install -y \
  apt-transport-https \
  apt-utils \
  build-essential \
  ca-certificates \
  dirmngr \
  gnupg \
  libcurl4-openssl-dev \
  libnlopt-dev \
  libssl-dev \
  libxml2-dev \
  lsb-release \
  software-properties-common 
  
ENV TZ=Europe/London
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9 && \
  add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/' && \
  apt-get update && \
  apt-get install -y \
  aptdaemon \
  cron \
  curl \
  ed \
  git \
  iputils-ping \
  libcairo-dev \
  libedit-dev \
  libgdal-dev \
  libgeos-dev \
  libjq-dev \
  libmagick++-dev \
  libprotobuf-dev \
  libudunits2-dev \
  libv8-dev \
  mercurial \
  nano \
  net-tools \
  protobuf-compiler \
  python3 \
  python3-pip \
  python3.6-venv \
  r-base \
  r-base-dev \
  sudo \
  supervisor \
  wget

RUN R -e "install.packages(repos='http://cran.uk.r-project.org', \
pkgs=c('animation',\
'broom',\
'DBI',\
'dbplyr',\
'devtools',\
'dplyr',\
'ggmap',\
'ggplot2',\
'hexbin',\
'knitr',\
'lme4',\
'magick',\
'png',\
'purrr',\
'raster',\
'rgdal',\
'rgeos',\
'rgeos',\
'RPostgreSQL',\
'RSQLite',\
'rvest',\
'shiny',\
'shinydashboard',\
'sp',\
'testthat',\
'tidyr',\
'tidyverse',\
'xml2'))"

RUN \
  python3 -m pip --no-cache-dir install pip --upgrade && \
  python3 -m pip --no-cache-dir install setuptools --upgrade && \
  python3 -m pip --no-cache-dir install wheel --upgrade && \
  python3 -m pip --no-cache-dir install jinja2 numpy pandas pytest sphinx tzlocal && \
  rm -rf /root/.cache

ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
RUN \
 apt-get update -qq && \
 apt-get install -y \
 libssl-dev \
 openjdk-8-jdk
  
# install Jupyter
RUN python3 -m pip install jupyter 

# Install some Python things
RUN apt-get update && \
    apt-get install -y \
    libcgal-dev \
    libfftw3-3  \
    libfftw3-dev \
    libfreetype6-dev \
    libftgl2 \
    libgtk2.0-dev \
    libglu1-mesa-dev \
    libsndfile1 \
    libsndfile1-dev \
    libx11-dev \
    pandoc \
    python-rpy2 \
    texlive-xetex 

# Install deep learning libraries
RUN pip3 install --upgrade pip setuptools
RUN pip3 install \
  boto \
  CherryPy \
  clickhouse-driver \
  folium \
  geohash2 \
  ipyleaflet \
  keras \
  matplotlib \
  pandahouse \
  pandahouse \
  psycopg2 \
  pymssql \
  pyodbc \
  pyspark \
  python-docx \
  rasterio \
  rpy2 \
  ruptures \
  sklearn \
  sqlalchemy \
  tensorflow \
  theano \
  tqdm \
  xlrd \
  XlsxWriter

RUN R CMD javareconf
RUN R -e "install.packages(repos = 'http://cran.uk.r-project.org', \
pkgs=c('acepack',\
'aqp',\
'checkmate',\
'classInt',\
'colorRamps',\ 
'cpm',\
'crayon',\
'data.table',\
'devtools',\
'DiagrammeR',\
'digest',\
'dismo',\
'DT',\
'dtw',\
'dygraphs',\
'e1071',\
'evaluate',\
'fftw',\
'FNN',\
'foreach',\
'forecast',\
'Formula',\
'gdalUtils',\
'geojson', \
'geojsonio',\
'ggcorrplot',\
'googleVis',\
'gstat',\
'hashFunction',\
'Hmisc',\
'htmlTable',\ 
'igraph',\
'intervals',\
'IRdisplay',\
'iterators',\
'keras',\
'latticeExtra',\
'leafem',\
'leaflet',\
'leafpop',\
'maptools',\
'mapview',\
'openxlsx',\
'pandoc',\
'pbdZMQ',\
'pixmap',\
'plotKML',\
'plotly',\
'plotrix',\ 
'quantmod',\
'R.methodsS3',\
'R.oo',\
'R.utils',\ 
'Rcpp',\
'repr',\
'reshape',\
'reticulate',\
'rJava',\
'RJDBC',\
'RJSONIO',\
'RSAGA',\
'satellite',\
'scatterplot3d',\
'seewave',\
'sf',\
'shapefiles',\
'solrium',\
'spacetime',\
'testthat',\
'timeseries',\
'tinytex',\
'tuneR',\
'units',\
'uuid',\
'validate',\
'viridis'))"

RUN R -e "tinytex::install_tinytex(force=T, extra_packages=c('beamer','ms','pgf','xcolor','translator', 'fancyhdr', 'eso-pic', 'comment', 'psnfss'))"

RUN apt-get update && \
  apt-get install -y libpoppler-cpp-dev
RUN R -e "install.packages(repos = 'http://cran.uk.r-project.org', \
  pkgs=c('Rdpack',\
  'bibtex',\
  'changepoint',\
  'changepoint.np',\
  'ecp',\
  'gbRd',\
  'gtools',\
  'pdftools'))"
  
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/ubuntu/18.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update
RUN ACCEPT_EULA=Y apt-get install -y msodbcsql17
RUN apt-get install -y unixodbc-dev unixodbc

RUN apt-get install -y software-properties-common && \
    add-apt-repository ppa:marutter/c2d4u3.5 && \
    apt-get update && \
    apt-get install -y r-cran-rgl ffmpeg

RUN Rscript -e "devtools::install_github('IRkernel/IRkernel')"
RUN Rscript -e "IRkernel::installspec(user = FALSE)"

RUN pip3 install jupyter_kernel_gateway

RUN pip3 install spylon-kernel
RUN python3 -m spylon_kernel install

RUN pip3 install jupyterlab

RUN pip3 install jupyter_contrib_nbextensions
RUN jupyter contrib nbextension install --user

RUN pip3 install RISE

ENV WORKINGFOLDER=/tmp/jupyter
ENV EXAMPLESFOLDER=$WORKINGFOLDER/examples
RUN mkdir -p $EXAMPLESFOLDER
COPY examples/ $EXAMPLESFOLDER/

RUN mkdir -p /var/log/supervisor
RUN echo "[supervisord]" >>/etc/supervisor/conf.d/supervisord.conf
RUN echo "autostart=true" >>/etc/supervisor/conf.d/supervisord.conf
RUN echo "autorestart=true" >>/etc/supervisor/conf.d/supervisord.conf
RUN echo "nodaemon=true" >>/etc/supervisor/conf.d/supervisord.conf
RUN echo "[program:crond]" >>/etc/supervisor/conf.d/supervisord.conf
RUN echo "command = /usr/sbin/cron -f" >>/etc/supervisor/conf.d/supervisord.conf
RUN echo "user=root" >>/etc/supervisor/conf.d/supervisord.conf
RUN echo "autostart=true" >>/etc/supervisor/conf.d/supervisord.conf
RUN echo "autorestart=true" >>/etc/supervisor/conf.d/supervisord.conf
RUN echo "[program:jupyter]" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "command = jupyter notebook --no-browser --ip='0.0.0.0' --notebook-dir=$WORKINGFOLDER --allow-root --NotebookApp.token=''" >>/etc/supervisor/conf.d/supervisord.conf
RUN echo "autostart=true" >>/etc/supervisor/conf.d/supervisord.conf
RUN echo "autorestart=true" >>/etc/supervisor/conf.d/supervisord.conf
RUN echo "[program:jupyterkernelgateway]" >> /etc/supervisor/conf.d/supervisord.conf
RUN echo "command = jupyter kernelgateway --ip='0.0.0.0' --port=8889 --api='kernel_gateway.notebook_http' --seed_uri='$EXAMPLESFOLDER/endpoint/ServiceEndpoint.ipynb'" >>/etc/supervisor/conf.d/supervisord.conf
RUN echo "autostart=true" >>/etc/supervisor/conf.d/supervisord.conf
RUN echo "autorestart=true" >>/etc/supervisor/conf.d/supervisord.conf

WORKDIR $WORKINGFOLDER

CMD ["/usr/bin/supervisord"]